<!DOCTYPE html>
<html>
<body>
<title>API</title>
<h1>My first PHP page</h1>

<?php

function fullName($fname, $lname) {
    echo "Full Name: $fname $lname <br>";
  }
  
  fullName("Jefferson", "Verar");


$hobbies = array("Playing Basketball", "Cooking", "Playing Video Games"); 
echo "My Hobbies are: " . $hobbies[0] . ", " . $hobbies[1] . " and " . $hobbies[2] . ".";

$age = 22;
echo "<br> My Age is: $age<br>";
$email = "jefferson.verar.pixel8@gmail.com";
echo "Contact me at: $email<br>";
echo "My Birthday is " . date("2001/02/21") . "<br>";
?>

</body>
</html>
